# Table of Contents

<!-- MarkdownTOC -->

- [What is this](#what-is-this)
- [Examples](#examples)
- [Advantages](#advantages)
- [Installation](#installation)
- [Usage](#usage)
- [Minor caveats](#minor-caveats)

<!-- /MarkdownTOC -->


# What is this

This is a very simple python script that I made so I can create [anki](https://github.com/dae/anki) cards in my favourite text editor (vim). The idea is that I always have a document (.tex) opened so I can easily create anki cards. At the end of the day (or whenever), I open anki and import the file that this script outputs. 

If you don't know what [anki](https://github.com/dae/anki) is, check it out. It's awesome!

# Examples

I have a file called "math.tex". I open that file with my favourite text editor and type the following:

```latex
q: What is the Pythagorean theorem?

The Pythagorean theorem states that $$a^2 + b^2 = c^2$$, where a, b and c are the sides in a right triangle.

---

q: What is the time independent Schrodinger equation?

It is given by

$$H\Psi = E \Psi$$
```

Lines that start with "q:" are questions. You separate questions by "---". Everything between "q:" and "---", even empty lines, are part of the answer (formatting reasons). 

# Advantages

The main advantage is that, by using a fully-featured text editor, you have snippets and auto-completion available! 

# Installation

This is a script, with no dependencies (other than python (edit: it has now "argparse" and "os" as dependencies)). Just add it to your PATH or add an alias in your .bashrc so you can call it from wherever. 

# Usage

An usage example would be:

```
to_anki input_file -o output_file
```

where "input_file" is the file where you have the information which you want to convert into cards and output_file is the file which you will import into anki. So, "input_file" is your text-editor's file, "output_file" is the file with the right format for inputing to anki.


# Minor caveats

- Having to manually import the info;

In the future, I hope to make use of the anki-connect and write to anki automatically.

- Each file in which you write should be for a different deck;

If I have a math deck and a "biology" deck, then I should have two files, "math.tex" and "biology.tex". This is because, when importing, you choose a deck for the whole file. When/If I support anki-connect, this should be solved as well, by a adding a "deck: name_of_deck" line.