import argparse
import os


def main():

	parser = argparse.ArgumentParser(description='Simple script to create anki cards in your favourite text editor.')
	# parser.add_argument('--input_file', '-i', help='Follow this argument with the input file.', nargs=1, type=str)
	# parser.add_argument('--output_file', '-o', help='Follow this argument with the output file.', nargs=1, type=str)
	parser.add_argument('input_file', help='Path to the input file.', nargs=1, type=str)
	parser.add_argument('--output_file', "-o", help='Path to the output file. If this argument is not given, the output fill will be "input_output.txt", where input is the name of the input file', nargs=1, type=str)
	args = parser.parse_args()

	print(args)

	input_file = args.input_file
	output_file = args.output_file

	print(output_file)
	if output_file is None:
		output_file = os.path.dirname(os.path.abspath(input_file[0])) + "/" + os.path.splitext(os.path.basename(input_file[0]))[0] + "_output.txt"
		print(output_file)
	else:
		output_file = os.path.abspath(str(output_file[0]))

	input_file = os.path.abspath(str(input_file[0]))

	print("Input_file:", input_file)
	print("Output_file:", output_file)

	card_string = ""

	with open(input_file) as file:
		for line in file:
			equation_mode = line.count("$")
			# if (equation_mode == 2) or (equation_mode == 4):
			if equation_mode > 1: #If it finds more than 1 $ in the string, it enters equation mode.
				new_string = replace_tex_chars(line, "$")
				line = new_string

			if not line.isspace():
				if line[0:2] == "q:":  # It's a question
					# This removes both the "q: " and the newline character.
					line = line[3:].rstrip("\n")
					card_string = card_string + line + "\t"
				elif line[0:3] != "---":
					card_string = card_string + \
						line.replace("\n", "<div><br></div><div>")
				else:
					card_string = card_string + "\n"
			# print(line.rstrip("\n"))

	with open(output_file, 'w') as file:
		file.write(card_string)


def replace_tex_chars(original_string, tex_marker):
	tex_marker_positions = []

	for i, char in enumerate(original_string):
		if char in tex_marker:
			tex_marker_positions.append(i)

	equation_type = []
	equation_part = []
	text_part = []
	# text_part.append(original_string[:tex_marker_positions[0]])
	# new_string = ""
	new_string = original_string[:tex_marker_positions[0]]
	j = 0
	counter_equation = 0
	counter_text = 0
	type_of_string = 0 ## Starts with text
	for i, element in enumerate(tex_marker_positions):
		if (i + 1) % 2 == 0:
			consecutive_marker = tex_marker_positions[i] - tex_marker_positions[i-1]
			if consecutive_marker == 1 and j % 2 == 0: # Big equation
				equation_part.append("[$$]" + original_string[tex_marker_positions[i]+1: tex_marker_positions[i+2]-1] + "[/$$]")
				j = j + 1 # Makes it so it only enters this if statement once per big equation
				type_of_string = 1
				counter_equation = counter_equation + 1
			elif consecutive_marker != 1: # Small equation
				equation_part.append("[$]" + original_string[tex_marker_positions[i-1]+1: tex_marker_positions[i]] + "[/$]")
				type_of_string = 2
				counter_equation = counter_equation + 1
		if (i + 1) % 2 != 0 and i > 0:
			consecutive_marker2 = tex_marker_positions[i+1] - tex_marker_positions[i]
			if consecutive_marker != consecutive_marker2:
				text_part.append(original_string[tex_marker_positions[i-1] + 1: tex_marker_positions[i]])
				type_of_string = 3
				counter_text = counter_text + 1
		if type_of_string == 1 or type_of_string == 2:
			new_string = new_string + equation_part[counter_equation-1]
		elif type_of_string == 3:
			new_string = new_string + text_part[counter_text-1]
		type_of_string = 0


	# text_part.append(original_string[tex_marker_positions[-1]:])

	new_string = new_string + original_string[tex_marker_positions[-1] + 1:]

	return new_string

if __name__ == '__main__':
	main()
